var express = require('express');
var consultar = require('../BD/consultar.js');
var router = express.Router();

//retorna la vista login.
router.post('/',function(req,res){
	var parametros = JSON.parse(Buffer.from(req.body.data, 'base64'));
  var strCorreo = parametros.correo;
  var strPassword = parametros.password;

  var boolEstado = false;
  var consulta = {};

  jsonRespuesta = {
    mensaje:"",
    ingreso: false,
    status: false
  };

  // consulta
  var strQuery = `SELECT
								u.idusuario, u.correo, u.nombre, u.nombre2 , u.apellido, u.apellido2
								FROM
								usuario u
								WHERE
								correo = ? AND password = ? `;

  // parametros para la consulta
  var parametrosQuery = [strCorreo, strPassword];

  consultar(strQuery,parametrosQuery, function(respuesta){
    boolEstado = respuesta.status;
    consulta = respuesta.data;
    console.log(consulta);

    if(consulta.length == 1 && boolEstado){
        sess = req.session;
        sess.usuario = consulta[0].idusuario;
        sess.correo = consulta[0].correo;
        sess.nombre = consulta[0].nombre;
        sess.nombre2= consulta[0].nombre2;
        sess.apellido = consulta[0].apellido;
        sess.apellido2 = consulta[0].apellido2;

        jsonRespuesta.mensaje = "Ingreso existoso";
        jsonRespuesta.ingreso = true;
        jsonRespuesta.status = true;
    }else{
      jsonRespuesta.mensaje = "El Usuario y/o la Contraseña están errados, por favor verifique su información";
      jsonRespuesta.ingreso = false;
      jsonRespuesta.status = true;
    }
    var strRespuesta = JSON.stringify(jsonRespuesta);
    // respuesta para el cliente
    res.send(new Buffer(strRespuesta).toString('base64'));
  });
});

module.exports = router;

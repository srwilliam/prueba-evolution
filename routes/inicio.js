var express = require('express');
var consultar = require('../BD/consultar.js');
var router = express.Router();

//retorna la vista inicio.
router.get('/',function(req,res){
  sess = req.session;
  if(sess.correo){
    res.render('inicio', { csrfToken: req.csrfToken(), title : "Inicio" });
  }
  else{
    res.redirect('/');
  }
});

//Consulta todas las tareas del usuario activo
//Envia true o false respecto a si fue existosa la consulta o no.
router.get('/consultarTareas',function(req,res){
  jsonRespuesta = {
    data:{},
    status: false
  };

  // consulta
  var strQuery = `SELECT
								*
								FROM
								tarea t
								WHERE
								idusuario = ? `;

  // parametros para la consulta
  var parametrosQuery = [sess.usuario];

  consultar(strQuery,parametrosQuery, function(respuesta){
    if(respuesta.status){
        jsonRespuesta.data = respuesta.data;
        jsonRespuesta.status = true;
    }else{
      jsonRespuesta.status = false;
    }
    var strRespuesta = JSON.stringify(jsonRespuesta);
    // respuesta para el cliente
    res.send(new Buffer(strRespuesta).toString('base64'));
  });
});

//Edita una tarea en la BD respecto a los adatos enviados (is, nombre, prioridad y fecha)
//Envia true o false respecto a si fue existosa la edición o no.
router.post('/editarTarea',function(req,res){
	var parametros = JSON.parse(Buffer.from(req.body.data, 'base64'));

  var strId = parametros.id;
  var strNombre = parametros.nombre;
  var strPrioridad = parametros.prioridad;
  var strFecha = parametros.fecha;

  var strRespuesta = "";

  jsonRespuesta = {
    status: false
  };

  // consulta
  var strQuery = `UPDATE
                  tarea SET
                  nombre = ?,
                  prioridad = ?,
                  vencimiento = ?
                  WHERE idtarea = ? `;

  // parametros para la consulta
  var parametrosQuery = [ strNombre, strPrioridad, strFecha, strId];

  consultar(strQuery,parametrosQuery, function(respuesta){
      console.log(respuesta);
      jsonRespuesta.status = respuesta.status;
      // respuesta para el cliente
      strRespuesta = JSON.stringify(jsonRespuesta);
      res.send(new Buffer(strRespuesta).toString('base64'));
  });

});

//Inserta una nueva tarea en la BD respecto alos datos enviados del cliente
//Envia true o false respecto a si fue existosa la inserción o no.
router.post('/crearTarea',function(req,res){
  sess = req.session;
	var parametros = JSON.parse(Buffer.from(req.body.data, 'base64'));

  var strNombre = parametros.nombre;
  var strPrioridad = parametros.prioridad;
  var strFecha = parametros.fecha;

  var strRespuesta = "";

  jsonRespuesta = {
    status: false
  };

  // consulta
  var strQuery = `INSERT INTO tarea (nombre, prioridad, vencimiento, idusuario) VALUES (?,?,?,?); `;

  // parametros para la consulta
  var parametrosQuery = [ strNombre, strPrioridad, strFecha, sess.usuario];

  consultar(strQuery,parametrosQuery, function(respuesta){
      console.log(respuesta);
      jsonRespuesta.status = respuesta.status;
      // respuesta para el cliente
      strRespuesta = JSON.stringify(jsonRespuesta);
      res.send(new Buffer(strRespuesta).toString('base64'));
  });

});

//Elimina una tarea en la BD respecto al id enviado
//Envia true o false respecto a si fue existosa la eliminación o no.
router.post('/eliminarTarea',function(req,res){
  sess = req.session;
	var parametros = JSON.parse(Buffer.from(req.body.data, 'base64'));

  var idTarea = parametros.id;

  var strRespuesta = "";

  jsonRespuesta = {
    status: false
  };

  // consulta
  var strQuery = `DELETE FROM tarea WHERE idtarea = ?; `;

  // parametros para la consulta
  var parametrosQuery = [ idTarea];

  consultar(strQuery,parametrosQuery, function(respuesta){
      console.log(respuesta);
      jsonRespuesta.status = respuesta.status;
      // respuesta para el cliente
      strRespuesta = JSON.stringify(jsonRespuesta);
      res.send(new Buffer(strRespuesta).toString('base64'));
  });

});

//cerrar sesión
router.get('/cerrarSesion',function(req,res){
  req.session.destroy(function(err) {
    if(err) {
      console.log(err);
    } else {
      res.redirect('/');
    }
  });
});

module.exports = router;


var express = require('express');
var consultar = require('../BD/consultar.js');
var router = express.Router();

//retorna la vista registro de usuarios.
router.get('/', function (req, res) {
  res.render('registroUsuarios', { csrfToken: req.csrfToken(), title: "Registro de usuarios" });
});

//Crea un nuevo usuario en la BD respecto a los datos enviados del cliente (nombre, nombre2, apellido, apellido2, contrase�a y correo)
//Envia true o false respecto a si fue existosa la inserci�n o no.
router.post('/crearUsuario', function (req, res) {
  sess = req.session;
  var parametros = JSON.parse(Buffer.from(req.body.data, 'base64'));

  var strNombre = parametros.nombre;
  var strNombre2 = parametros.nombre2;
  var strApellido = parametros.apellido;
  var strApellido2 = parametros.apellido2;
  var strCorreo = parametros.correo;
  var strPassword = parametros.password;

  var strRespuesta = "";

  jsonRespuesta = {
    status: false
  };

  if (noNulo(strNombre) || noNulo(strApellido) || noNulo(strCorreo) || noNulo(strPassword)) {
    strRespuesta = JSON.stringify(jsonRespuesta);
    res.send(new Buffer(strRespuesta).toString('base64'));
  }

  // consulta si el correo ingresado ya existe.
  var strQuery = `SELECT * FROM usuario where correo = ?`;

  // parametros para la consulta
  var parametrosQuery = [strCorreo];

  consultar(strQuery, parametrosQuery, function (respuesta) {
    // console.log('Consulta 1::',respuesta);
    
    if(respuesta.data.length != 0){
      jsonRespuesta.mensaje = "Una cuenta ya ha sido creada con este correo.";
      strRespuesta = JSON.stringify(jsonRespuesta);
      res.send(new Buffer(strRespuesta).toString('base64'));
    }
    else{
      // Insertar el usuario en BD
      var strQuery = `INSERT INTO usuario (nombre, nombre2, apellido, apellido2, correo, password) VALUES (?,?,?,?,?,?); `;
  
      // parametros para la inserción
      var parametrosQuery = [strNombre, strNombre2, strApellido, strApellido2, strCorreo, strPassword];
  
      consultar(strQuery, parametrosQuery, function (respuesta) {
        // console.log(respuesta);
        jsonRespuesta.status = respuesta.status;
        // respuesta para el cliente
        strRespuesta = JSON.stringify(jsonRespuesta);
        res.send(new Buffer(strRespuesta).toString('base64'));
      });
    }

  });


});

function noNulo(param) {
  return param == null || param == undefined || param == "";
}

module.exports = router;

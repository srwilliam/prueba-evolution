var idtarea = "";
var tareas_pendientes = [];

$(document).ready(function() {
  $.fn.dataTable.moment( "YYYY/MM/DD HH:mm:ss" );
  consultarTareas();
});

$(document).off('click', '#editar').on('click', '#editar', function() {
  idTarea = $(this).data('id');
  var nombre = $(this).data('nombre');
  var prioridad = $(this).data('prioridad');
  var fecha = moment($(this).data('fecha')).format('DD-MM-YYYYTHH:mm');

  $('#nombre').val(nombre);
  $('#prioridad').val(prioridad);
  $('#vencimiento').val(fecha);

  $("#formularioModal").modal('show');
});

$(document).off('click', '#eliminar').on('click', '#eliminar', function() {
  idTarea = $(this).data('id');
  eliminarTarea();
});

$(document).off('click', '#crear').on('click', '#crear', function() {
  $('#nombreCrear').val('');
  $('#prioridadCrear').val('');
  $('#vencimientoCrear').val('');

  $("#formularioModalCrear").modal('show');
});

$(document).off('click', '#btnGuardarEditar').on('click', '#btnGuardarEditar', function() {
  editarTarea();
});

$(document).off('click', '#btnGuardarCrear').on('click', '#btnGuardarCrear', function() {
  crearTarea();
});

$(document).off('click', '#cerrarSesion').on('click', '#cerrarSesion', function() {
  $.get("inicio/cerrarSesion",{
    },function(data){
      location.reload();
    });
});

function consultarTareas() {
  //variable con los headers de la tabla
  var strFilas = `
  <thead>
      <tr>
          <th>Prioridad</th>
          <th>Nombre</th>
          <th>Vencimiento</th>
          <th>Operaciones</th>
      </tr>
  </thead>
  <tbody>`;

  $.get("inicio/consultarTareas",{
    },
    function(data){
      //parseo repuesta del servidor Base64->UTF8->Json
      var jsnData = $.parseJSON(b64_to_utf8(data));
      //check si la respuesta del servidor es válida
      if(jsnData.status){
        if(jsnData.data.length != 0){
          //quitar la funcionalidad del dataTable
          $("#tareasDatatable").dataTable().fnDestroy();
          //recorremos el array de datos
          jsnData.data.forEach(function(e,i) {
            var fecha = moment(e.vencimiento).format('YYYY/MM/DD hh:mm:ss');

            if(compararFechas(fecha)){
              tareas_pendientes.push(e.nombre);
            }

            //agregamos filas para la tabla
            strFilas += `<tr>`;
    				strFilas += `<td>${e.prioridad}</td>`;
            strFilas += `<td>${e.nombre}</td>`;
            strFilas += `<td>${fecha}</td>`;
            strFilas += "<td><div style='text-align: center;'>";
    				strFilas += `<i id = 'editar' data-id="${e.idtarea}" data-nombre="${e.nombre}" data-prioridad="${e.prioridad}"
                          data-fecha="${fecha}" style='color:black;margin-left: 3%;font-size: 25px;' data-toggle='tooltip'
                          class='fa fa-edit' title='Editar'></i>`;
    				strFilas += `<i id = 'eliminar' data-id="${e.idtarea}" style='color:black;margin-left: 3%;font-size: 25px;'
                        data-toggle='tooltip' class='fa fa-trash-o title='Eliminar'></i>`;
            strFilas += "</div></td>";
            strFilas += "</tr>";
          });
          strFilas += `</tbody>`;

          //colocamos las filas en la tabla
          $("#tareasDatatable").html(strFilas);
          //colocamos la funcionalidad DataTables
          $("#tareasDatatable").DataTable({
            responsive:true,
                "columnDefs": [
                  { "orderable": false, "targets": 3 } //deshabilitar el orden en la cuarta columna
                ]
          });
          //tooltip para los iconos de la tabla
          $('[data-toggle="tooltip"]').tooltip({
              container: 'body'
          });

          if(tareas_pendientes!=0){
            swal({
              title: 'Tareas próximas a finalizar.',
              text: tareas_pendientes.toString(),
              icon: 'warning',
              button: {
                text: 'Entendido',
                value: true
              }
            });
          }

          tareas_pendientes = [];
        }else{
          $("#tareasDatatable").html(strFilas);
        }
      }else{
        swal("Ha ocurrido un error.");
      }
    }
  );
}

function editarTarea() {
  var strNombre = $('#nombre').val();
  var strPrioridad = $('#prioridad').val();
  var verificacion = moment($('#vencimiento').val(), 'YYYY-MM-DD HH:mm:ss');
  var strFecha = verificacion.isValid() ? verificacion.format('YYYY-MM-DD HH:mm:ss'): "";

  //verificar campos no vacíos
  if (strNombre != "" && strPrioridad != "" && strFecha != "") {

    var data = {
      id : idTarea,
      nombre: strNombre,
      prioridad: strPrioridad,
      fecha: strFecha
    };

    var stringData = utf8_to_b64(JSON.stringify(data));

    $.ajax({
      type: "POST",
      url: "inicio/editarTarea",
      data: {
        data: stringData
      },
      headers: {
        'X-CSRF-TOKEN': csrfToken
      }
    }).done(function(data) {
      var respuesta = $.parseJSON(b64_to_utf8(data));

      if (respuesta.status) {
        swal({
          title: 'Edición',
          text: "Guardado correctamente.",
          icon: 'success',
          button: {
            text: 'Entendido',
            value: true
          }
        });
        $("#formularioModal").modal('hide');
        consultarTareas();
      }
      else {
        swal({
          title: 'Error',
          text: "Ha ocurrido un error.",
          icon: 'warning',
          button: {
            text: 'Entendido',
            value: true
          }
        });
      }

    }).fail(function(data) {
      alert("La operación no ha podido ser completada, verifique su conexión a internet.");
    });


  } else {
    //mensaje si no se han diliganciado bien los campos
    swal({
      title: 'Error',
      text: "Todos los campos deben estar diligenciados",
      icon: 'warning',
      button: {
        text: 'Entendido',
        value: true
      }
    });
  }
}

function crearTarea(){
  var strNombre = $('#nombreCrear').val();
  var strPrioridad = $('#prioridadCrear').val();
  var verificacion = moment($('#vencimientoCrear').val(), 'YYYY-MM-DD HH:mm:ss');
  var strFecha = verificacion.isValid() ? verificacion.format('YYYY-MM-DD HH:mm:ss'): "";

  //verificar campos no vacíos
  if (strNombre != "" && strPrioridad != "" && strFecha != "") {

    var data = {
      nombre: strNombre,
      prioridad: strPrioridad,
      fecha: strFecha
    };

    var stringData = utf8_to_b64(JSON.stringify(data));

    $.ajax({
      type: "POST",
      url: "inicio/crearTarea",
      data: {
        data: stringData
      },
      headers: {
        'X-CSRF-TOKEN': csrfToken
      }
    }).done(function(data) {
      var respuesta = $.parseJSON(b64_to_utf8(data));

      if (respuesta.status) {
        swal({
          title: 'Creación',
          text: "Guardado correctamente.",
          icon: 'success',
          button: {
            text: 'Entendido',
            value: true
          }
        });
        $("#formularioModalCrear").modal('hide');
        consultarTareas();
      }
      else {
        swal({
          title: 'Error',
          text: "Ha ocurrido un error.",
          icon: 'warning',
          button: {
            text: 'Entendido',
            value: true
          }
        });
      }

    }).fail(function(data) {
      alert("La operación no ha podido ser completada, verifique su conexión a internet.");
    });
  } else {
    //mensaje si no se han diliganciado bien los campos
    swal({
      title: 'Error',
      text: "Todos los campos deben estar diligenciados",
      icon: 'warning',
      button: {
        text: 'Entendido',
        value: true
      }
    });
  }
}

function eliminarTarea(){
  var data = {
    id: idTarea,
  };

  var stringData = utf8_to_b64(JSON.stringify(data));

  $.ajax({
    type: "POST",
    url: "inicio/eliminarTarea",
    data: {
      data: stringData
    },
    headers: {
      'X-CSRF-TOKEN': csrfToken
    }
  }).done(function(data) {
    var respuesta = $.parseJSON(b64_to_utf8(data));

    if (respuesta.status) {
      swal({
        title: 'Eliminación',
        text: "Eliminado correctamente.",
        icon: 'success',
        button: {
          text: 'Entendido',
          value: true
        }
      });
      consultarTareas();
    }
    else {
      swal({
        title: 'Error',
        text: "Ha ocurrido un error.",
        icon: 'warning',
        button: {
          text: 'Entendido',
          value: true
        }
      });
    }

  }).fail(function(data) {
    alert("La operación no ha podido ser completada, verifique su conexión a internet.");
  });
}

//Compara si en los próximos 3 días la fecha indicada terminará
//retorna true o false dependiendo si termina o no.
function compararFechas(fecha) {
    fecha = new Date(fecha);
    var date = new Date();
    date.setDate(date.getDate() - 3);
    return fecha > date && fecha < date.setDate(date.getDate() + 3);
}

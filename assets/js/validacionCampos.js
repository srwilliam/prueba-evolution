//Validación para nombre de usuario
function ValidarNombreUsuario(id) {
    $(id).ValidarCampos('1234567890abcdefghijklmnñopqrstuvwxyzáéíóúü_-');
}

//Validación para correo
function ValidarCorreo(id) {
    $(id).ValidarCampos('abcdefghijklmnñopqrstuvwxyzáéíóúü1234567890 -_.@');
}

//Permite la validación de textos
function ValidarTexto(id) {
    $(id).ValidarCampos('abcdefghijklmnñopqrstuvwxyzáéíóúü1234567890 -_.,;:@()/@!¡?¿#$%&');
}

(function (a) {
    a.fn.ValidarCampos = function (b) {
        a(this).on({
            keypress: function (a) {
                var c = a.which,
                    d = a.keyCode,
                    e = String.fromCharCode(c).toLowerCase(),
                    f = b;
                (-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || a.preventDefault()
            }
        });
        a(this).on({
            change: function (a) {
                var texto = "";
                for (var i = 0; i < this.value.length; i++) {
                    if (b.indexOf(this.value.toLowerCase().charAt(i), 0) != -1) {
                        texto = texto + this.value.charAt(i);
                    }
                }
                this.value = $.trim(texto);
            }
        });
    }
})(jQuery);

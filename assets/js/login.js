$(document).ready(function() {
  validarCampos();
});

function validarCampos() {
  ValidarCorreo('#correo');
  ValidarTexto('#password');
}

$(document).off('click', '#btn-inicio').on('click', '#btn-inicio', function() {
  iniciarSesion();
});

//iniciar sesión
//Envia el contenido de los inputs correo y contraseña al servidor
//Recibe una true o false dependiendo si el usuario puede ingresar o no
function iniciarSesion() {

  var strCorreo = $('#correo').val();
  var strPassword = $('#password').val();

  //verificar campos no vacíos
  if (strCorreo != "" && strPassword != "") {

    var data = {
      correo: strCorreo,
      password: strPassword
    };

    var stringData = utf8_to_b64(JSON.stringify(data));

    $.ajax({
      type: "POST",
      url: "/login",
      data: {
        data: stringData
      },
      headers: {
        'X-CSRF-TOKEN': csrfToken
      }
    }).done(function(data) {
      var respuesta = $.parseJSON(b64_to_utf8(data));

      if (respuesta.status) {
        console.log(respuesta);
        if (respuesta.ingreso) {
          var user = respuesta.data;
          window.location.href="/inicio";
        } else {

          $('#correo').val('');
          $('#password').val('');

          swal({
            title: 'Error',
            text: respuesta.mensaje,
            icon: 'warning',
            button: {
              text: 'Entendido',
              value: true
            }
          });

        }
      }
      else {
        swal({
          title: 'Error',
          text: "Ha ocurrido un error.",
          icon: 'warning',
          button: {
            text: 'Entendido',
            value: true
          }
        });
      }

    }).fail(function(data) {
      alert("La operación no ha podido ser completada, verifique su conexión a internet.");
    });


  } else {
    //mensaje si no se han diliganciado bien los campos
    swal({
      title: 'Error',
      text: "Todos los campos deben estar diligenciados",
      icon: 'warning',
      button: {
        text: 'Entendido',
        value: true
      }
    });
    //habilitar botón entrar
    $('#botonEntrar').prop('disabled', false);
  }
}

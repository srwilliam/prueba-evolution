var conexion = require('./conexionSQL.js');

//Realiza consultas a la base datos
//Recibe consultas SQL en forma de string, sus parámetros en un array y los retorna como un JSON
function ejecutar(strQuery,arrParametros,callback){

  var objResultado = {status:false, data:[]};
    // ejecución del query con sus parametros
    conexion.query(strQuery, arrParametros, function(err, rows) {
      // Si hay un error en la consulta
      if (err) {
        console.error('Error en la consulta: ' +err.stack);
        // La consulta no fue exitosa entonces false
        objResultado.status = false;

        callback(objResultado);
      }

      //Se guarda resultado de la consulta
      objResultado.data = rows;
      //Fue existosa la consulta
      objResultado.status = true;

      callback(objResultado);
    });
}

module.exports = ejecutar;
